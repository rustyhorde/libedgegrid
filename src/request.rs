//! Akamai {OPEN} EdgeGrid HTTP Request Handler
use auth;
use curl::http;
use url::percent_encoding as pe;

use self::HttpRequestVerb::*;

#[derive(Clone,Copy,Debug)]
#[allow(dead_code)]
pub enum HttpRequestVerb {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    TRACE,
    OPTIONS,
    CONNECT,
    PATCH,
}

pub trait EncodeBody {
    fn encode(&self) -> Result<String, ::EdgeGridError>;
}

pub fn request(egr: &auth::EdgeGridAuth,
               relurl: &str,
               body: Option<&str>,
               verb: HttpRequestVerb)
               -> ::EdgeGridResponse {
    // Setup the URL
    let newrel = &pe::utf8_percent_encode(relurl, pe::QUERY_ENCODE_SET)[..];
    let mut url = String::from(egr.baseurl());
    url.push_str(newrel);
    debug!("URL: {}", url);

    // Generate the Authorization header.
    let header = try!(egr.auth_header(verb, newrel, body));

    // curl handle and request
    let mut handle = http::handle().timeout(egr.timeout());
    let request = match verb {
        GET => handle.get(&url[..]),
        POST => {
            match body {
                Some(b) => handle.post(&url[..], b.as_bytes()),
                None => handle.post(&url[..], ""),
            }
        }
        rt => {
            return Err(::EdgeGridError {
                title: Some(format!("{:?} Request Type not implemented!", rt)),
                detail: None,
                ..Default::default()
            })
        }
    };

    // execute the curl command
    Ok(try!(request.header("Content-Type", "application/json")
                   .header("Authorization", &header[..])
                   .exec()))
}
