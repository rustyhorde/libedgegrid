use auth::EdgeGridAuth;
use request;
use request::HttpRequestVerb::*;
use serde_json;

#[cfg(feature = "serde_macros")]
include!("req.rs.in");

#[cfg(not(feature = "serde_macros"))]
include!(concat!(env!("OUT_DIR"), "/luna/alert/req.rs"));

impl AlertPostData {
    pub fn new(cp_codes: Option<Vec<String>>) -> AlertPostData {
        let mut codes_str = String::new();
        let codes = match cp_codes {
            Some(ref cc) => {
                for cp_code in cc.iter() {
                    codes_str.push_str(cp_code);
                    codes_str.push_str(",");
                }

                Some(String::from(codes_str.trim_right_matches(",")))
            }
            None => None,
        };
        AlertPostData {
            status: String::from("active"),
            cp_codes: codes,
        }
    }
}

pub fn get(edge_grid_auth: &EdgeGridAuth, cp_codes: &Option<Vec<String>>) -> ::EdgeGridResponse {
    let mut relurl = format!("/alerts/v1/portal-user?status=active");

    let url_slice = match *cp_codes {
        Some(ref cc) => {
            relurl.push_str("&cpCodes=");

            for cp_code in cc.iter() {
                relurl.push_str(cp_code);
                relurl.push_str(",");
            }

            relurl.trim_right_matches(",")
        }
        None => &relurl[..],
    };
    Ok(try!(request::request(edge_grid_auth, url_slice, None, GET)))
}

pub fn post(edge_grid_auth: &EdgeGridAuth, body: AlertPostData) -> ::EdgeGridResponse {
    let relurl = format!("/alerts/v1/portal-user");
    let json = &try!(serde_json::to_string(&body))[..];
    Ok(try!(request::request(edge_grid_auth, &relurl[..], Some(json), POST)))
}

pub fn get_by_id(edge_grid_auth: &EdgeGridAuth, alert_id: &String) -> ::EdgeGridResponse {
    let relurl = format!("/alerts/v1/portal-user/alert/{}", alert_id);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}
