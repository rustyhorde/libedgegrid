use curl::http;
use std::fmt;

use self::AlertResponseType::*;

pub enum AlertResponseType {
    Get,
    GetById,
    Post,
}

#[cfg(feature = "serde_macros")]
include!("resp.rs.in");

#[cfg(not(feature = "serde_macros"))]
include!(concat!(env!("OUT_DIR"), "/luna/alert/resp.rs"));

pub fn parse(resp: http::Response, resp_type: AlertResponseType) -> ::EdgeGridResult {
    match resp_type {
        Get | Post => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<AlertsResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        GetById => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<AlertDetailsResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
    }
}
