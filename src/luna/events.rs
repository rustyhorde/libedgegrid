//! Events API
use curl;
use auth::EdgeGridAuth;
use request::HttpRequestVerb::*;
use url::percent_encoding as pe;
use rustc_serialize::json;
use std::borrow::Borrow;

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Filter {
    endRange: Option<usize>,
    startRange: Option<usize>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Contents {
    resultsPerPage: Option<usize>,
    currentPage: Option<usize>,
    totalResultsCount: Option<usize>,
    totalPageCount: Option<usize>,
    sortKey: Option<String>,
    sortOrder: Option<String>,
    filters: Option<Filter>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Customer {
    accountID: Option<String>,
    accountName: Option<String>,
    supportLevel: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Tag {
    id: Option<usize>,
    name: Option<String>,
    global: Option<bool>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct DataSet {
    id: Option<usize>,
    customerEventId: Option<String>,
    version: Option<usize>,
    name: Option<String>,
    description: Option<String>,
    eventType: Option<String>,
    customer: Option<Customer>,
    start: Option<usize>,
    end: Option<usize>,
    createdDate: Option<usize>,
    createdBy: Option<String>,
    modifiedDate: Option<usize>,
    modifiedBy: Option<String>,
    audienceSize: Option<usize>,
    audienceLocations: Option<Vec<String>>,
    coordinators: Option<Vec<String>>,
    locked: Option<bool>,
    tags: Option<Vec<Tag>>,
    services: Option<String>,
    supportType: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct EventsResponse {
    status: Option<String>,
    contents: Option<Contents>,
    dataSet: Option<Vec<DataSet>>,
}

pub fn get_by_account_id(egr: &EdgeGridAuth,
                         account_id: &str)
                         -> Result<EventsResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/events/v2/{}/events", account_id);

    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(GET, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .get(&url[..])
                        .header("Content-Type", "application/json")
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: EventsResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}
