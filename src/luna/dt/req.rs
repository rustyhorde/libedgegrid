use auth::EdgeGridAuth;
use request;
use request::HttpRequestVerb::*;

#[derive(Debug)]
pub enum DigQueryType {
    A,
    CNAME,
    MX,
}

impl<'a> From<&'a str> for DigQueryType {
    fn from(qt: &'a str) -> DigQueryType {
        match qt {
            "A" => DigQueryType::A,
            "CNAME" => DigQueryType::CNAME,
            "MX" => DigQueryType::MX,
            _ => panic!("Unknown query type!"),
        }
    }
}

pub fn dig(edge_grid_auth: &EdgeGridAuth,
           host: &str,
           loc: &str,
           query_type: &str)
           -> ::EdgeGridResponse {
    let qt = DigQueryType::from(query_type);
    let mut relurl = String::from("/diagnostic-tools/v1/dig");
    relurl.push_str("?");
    relurl.push_str(&format!("hostname={}&", host)[..]);
    relurl.push_str(&format!("location={}&", loc)[..]);
    relurl.push_str(&format!("queryType={:?}", qt)[..]);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}

pub fn error_translate(edge_grid_auth: &EdgeGridAuth, error_code: &str) -> ::EdgeGridResponse {
    let mut relurl = String::from("/diagnostic-tools/v1/errortranslator");
    relurl.push_str(&format!("?errorCode={}", error_code)[..]);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}

pub fn geo(edge_grid_auth: &EdgeGridAuth, ip: &str) -> ::EdgeGridResponse {
    let mut relurl = String::from("/diagnostic-tools/v1/ipgeolocator");
    relurl.push_str(&format!("?ip={}", ip)[..]);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}

pub fn locations(edge_grid_auth: &EdgeGridAuth) -> ::EdgeGridResponse {
    let relurl = "/diagnostic-tools/v1/locations";
    Ok(try!(request::request(edge_grid_auth, relurl, None, GET)))
}

pub fn mtr(edge_grid_auth: &EdgeGridAuth, dest: &str, loc: &str) -> ::EdgeGridResponse {
    let mut relurl = String::from("/diagnostic-tools/v1/mtr");
    relurl.push_str("?");
    relurl.push_str(&format!("destinationDomain={}&", dest)[..]);
    relurl.push_str(&format!("location={}", loc)[..]);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}

pub fn translate(edge_grid_auth: &EdgeGridAuth, url: &str) -> ::EdgeGridResponse {
    let mut relurl = String::from("/diagnostic-tools/v1/akamaitranslator");
    relurl.push_str(&format!("?hostname={}", url)[..]);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}

pub fn verify(edge_grid_auth: &EdgeGridAuth, ip: &str) -> ::EdgeGridResponse {
    let mut relurl = String::from("/diagnostic-tools/v1/verifycdnip");
    relurl.push_str(&format!("?ip={}", ip)[..]);
    Ok(try!(request::request(edge_grid_auth, &relurl[..], None, GET)))
}
