use curl::http;
use std::fmt;

use self::DTResponseType::*;

pub enum DTResponseType {
    Dig,
    ErrorTranslate,
    Geo,
    Locations,
    Mtr,
    Translate,
    Verify,
}

#[cfg(feature = "serde_macros")]
include!("resp.rs.in");

#[cfg(not(feature = "serde_macros"))]
include!(concat!(env!("OUT_DIR"), "/luna/dt/resp.rs"));

pub fn parse(resp: http::Response, dt_type: DTResponseType) -> ::EdgeGridResult {
    match dt_type {
        Dig => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<DigResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        ErrorTranslate => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<ErrorTranslateResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        Geo => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<IPGeoLocationResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        Locations => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<LocationsResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        Mtr => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<MtrResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        Translate => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<ArlResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
        Verify => {
            ::response::parse_response(resp,
                                       |b| ::response::gen_output::<VerifyIPCDNResponse>(b),
                                       |b| ::response::gen_error_output(b))
        }
    }
}
