//! Log Delivery Service API
use curl;
use auth::EdgeGridAuth;
use request::HttpRequestVerb::*;
use url::percent_encoding as pe;
use rustc_serialize::json;
use std::borrow::Borrow;

#[derive(Debug, RustcDecodable, RustcEncodable)]
#[allow(non_snake_case)]
pub struct EmailConfig {
    emailAddress: Option<String>,
}

#[derive(Debug, RustcDecodable, RustcEncodable)]
#[allow(non_snake_case)]
pub struct FtpConfig {
    machine: Option<String>,
    login: Option<String>,
    password: Option<String>,
    directory: Option<String>,
}

#[derive(Debug, Default, RustcDecodable, RustcEncodable)]
#[allow(non_snake_case)]
pub struct Contact {
    contactEmail: Option<Vec<String>>,
    dictId: Option<String>,
}

#[derive(Debug, Default, RustcDecodable, RustcEncodable)]
#[allow(non_snake_case)]
pub struct DictId {
    dictId: Option<String>,
}

#[derive(Debug, Default, RustcDecodable, RustcEncodable)]
#[allow(non_snake_case)]
pub struct AcgObject {
    id: Option<usize>,
    name: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Zone {
    fixed: Option<bool>,
    id: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Chronology {
    zone: Option<Zone>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct Moment {
    year: Option<usize>,
    dayOfMonth: Option<usize>,
    dayOfWeek: Option<usize>,
    era: Option<usize>,
    dayOfYear: Option<usize>,
    weekyear: Option<usize>,
    monthOfYear: Option<usize>,
    yearOfEra: Option<usize>,
    yearOfCentury: Option<usize>,
    centuryOfEra: Option<usize>,
    millisOfSecond: Option<usize>,
    millisOfDay: Option<usize>,
    secondOfMinute: Option<usize>,
    secondOfDay: Option<usize>,
    minuteOfHour: Option<usize>,
    minuteOfDay: Option<usize>,
    hourOfDay: Option<usize>,
    weekOfWeekyear: Option<usize>,
    millis: Option<usize>,
    zone: Option<Zone>,
    chronology: Option<Chronology>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct ConfigContent {
    serviceId: Option<usize>,
    deliveryType: Option<String>,
    emailConfiguration: Option<EmailConfig>,
    ftpConfiguration: Option<FtpConfig>,
    contact: Option<Contact>,
    status: Option<String>,
    startDate: Option<Moment>,
    endDate: Option<Moment>,
    logFormat: Option<DictId>,
    logIdentifier: Option<String>,
    aggregationType: Option<String>,
    deliveryFrequency: Option<DictId>,
    deliveryThreshold: Option<String>,
    deliverResidual: Option<usize>,
    messageSize: Option<DictId>,
    encoding: Option<DictId>,
    encodingKey: Option<String>,
    configurationType: Option<String>,
    acgObject: Option<AcgObject>,
    productGroupId: Option<usize>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct ConfigsResponse {
    status: Option<String>,
    contents: Option<Vec<ConfigContent>>,
    errorCode: Option<String>,
    incidentId: Option<String>,
    errorType: Option<String>,
    errorMessage: Option<String>,
    resolution: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct ConfigResponse {
    status: Option<String>,
    contents: Option<ConfigContent>,
    errorCode: Option<String>,
    incidentId: Option<String>,
    errorType: Option<String>,
    errorMessage: Option<String>,
    resolution: Option<String>,
}

#[derive(Default, RustcEncodable)]
#[allow(non_snake_case)]
pub struct ConfigPutPostData {
    configurationType: String,
    actObject: AcgObject,
    productGroupId: usize,
    startDate: usize,
    endDate: Option<usize>,
    logFormat: DictId,
    logIdentifier: String,
    aggregationType: String,
    deliveryType: String,
    emailConfiguration: Option<EmailConfig>,
    ftpConfiguration: Option<FtpConfig>,
    deliveryFrequency: Option<DictId>,
    deliveryThreshold: Option<String>,
    deliveryResidualFlag: Option<bool>,
    messageSize: DictId,
    encoding: DictId,
    encodingKey: Option<String>,
    contact: Contact,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct BasicResponse {
    status: Option<String>,
    contents: Option<String>,
    errorCode: Option<String>,
    incidentId: Option<String>,
    errorType: Option<String>,
    errorMessage: Option<String>,
    resolution: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct KeyValueData {
    key: Option<String>,
    value: Option<String>,
}

#[derive(Debug, RustcDecodable)]
#[allow(non_snake_case)]
pub struct KeyValueResponse {
    status: Option<String>,
    contents: Option<Vec<KeyValueData>>,
    errorCode: Option<String>,
    incidentId: Option<String>,
    errorType: Option<String>,
    errorMessage: Option<String>,
    resolution: Option<String>,
}

#[derive(RustcEncodable)]
#[allow(non_snake_case)]
pub struct TargetData {
    acgObjectId: String,
    acgObjectType: String,
    productGroupId: usize,
}


#[derive(RustcEncodable)]
#[allow(non_snake_case)]
pub struct CopyPostData {
    target: TargetData,
}

pub fn get_configs(egr: &EdgeGridAuth) -> Result<ConfigsResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = String::from("/lds/v1/configurations");
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(GET, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .get(&url[..])
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: ConfigsResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn get_config(egr: &EdgeGridAuth, service_id: &str) -> Result<ConfigResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(GET, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .get(&url[..])
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: ConfigResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn post_config(egr: &EdgeGridAuth,
                   service_id: &str,
                   post_data: &ConfigPutPostData)
                   -> Result<BasicResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let post_json = try!(json::encode(post_data));
    let header = try!(egr.auth_header(POST, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .post(&url[..], post_json.as_bytes())
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: BasicResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn put_config(egr: &EdgeGridAuth,
                  service_id: &str,
                  put_data: &ConfigPutPostData)
                  -> Result<BasicResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let put_json = try!(json::encode(put_data));
    let header = try!(egr.auth_header(PUT, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .put(&url[..], put_json.as_bytes())
                        .header("Content-Type", "application/json")
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: BasicResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn delete_config(egr: &EdgeGridAuth, service_id: &str) -> Result<BasicResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(DELETE, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .delete(&url[..])
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: BasicResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn copy_config(egr: &EdgeGridAuth,
                   service_id: &str,
                   obj_id: &str,
                   obj_type: &str,
                   pg_id: usize)
                   -> Result<BasicResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/copy/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let post_data = CopyPostData {
        target: TargetData {
            acgObjectId: String::from(obj_id),
            acgObjectType: String::from(obj_type),
            productGroupId: pg_id,
        },
    };
    let post_json = try!(json::encode(&post_data));
    let header = try!(egr.auth_header(POST, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .post(&url[..], post_json.as_bytes())
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: BasicResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn suspend_config(egr: &EdgeGridAuth, service_id: &str) -> Result<BasicResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/suspend/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(POST, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .post(&url[..], "")
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: BasicResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn resume_config(egr: &EdgeGridAuth, service_id: &str) -> Result<BasicResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let relurl = format!("/lds/v1/configurations/resume/{}", service_id);
    url.push_str(&relurl[..]);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(POST, &relurl[..], None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .post(&url[..], "")
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: BasicResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}

pub fn get_dict<'a>(egr: &EdgeGridAuth,
                    dict_name: &str,
                    object_type: Option<&String>,
                    object_id: Option<usize>,
                    current_key_value: Option<&String>)
                    -> Result<KeyValueResponse, ::ApiError> {
    let mut url = String::from(egr.baseurl());
    let mut relurl = format!("/lds/v1/dictionaries/{}", dict_name);

    if object_type.is_some() || object_id.is_some() || current_key_value.is_some() {
        relurl.push_str("?");
    }

    if object_type.is_some() {
        let otarg = format!("objectType={}&", object_type.unwrap());
        relurl.push_str(&otarg[..]);
    }

    if object_id.is_some() {
        let oidarg = format!("objectId={}&", object_id.unwrap());
        relurl.push_str(&oidarg[..]);
    }

    if current_key_value.is_some() {
        let ckvarg = format!("productGroupId={}", current_key_value.unwrap());
        relurl.push_str(&ckvarg[..]);
    }

    let relurlt = relurl.trim_right_matches('&');

    url.push_str(relurlt);
    url = pe::utf8_percent_encode(&url[..], pe::QUERY_ENCODE_SET);
    let header = try!(egr.auth_header(GET, relurlt, None));
    debug!("URL: {}", url);
    let resp = try!(curl::http::handle()
                        .timeout(egr.timeout())
                        .get(&url[..])
                        .header("Authorization", &header[..])
                        .exec());
    let body = String::from_utf8_lossy(resp.get_body());
    debug!("Status: {}", resp.get_code());
    debug!("Body: {}", body);

    match resp.get_code() {
        200 => {
            let jsonresp: KeyValueResponse = try!(json::decode(&body));
            Ok(jsonresp)
        }
        _ => {
            let json_err: ::JSONError = try!(json::decode(&body));
            Err(::ApiError {
                json_err: json_err,
                body: String::from(body.borrow()),
            })
        }
    }
}
