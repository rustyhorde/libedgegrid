//! Akamai {OPEN} EdgeGrid API Request/Response Handler
//!
//! [Akamai {OPEN}]
//! [akamai {open}]: https://developer.akamai.com/
//!
//! * [Content Control Utility]
//! [content control utility]: https://developer.akamai.com/api/purge/ccu/overview.html
//! * [Diagnostic Tools]
//! [diagnostic tools]: https://developer.akamai.com/api/luna/diagnostic-tools/overview.html
//! * [Alerts]
//! [alerts]: https://developer.akamai.com/api/luna/alerts/overview.html

// Copyright (c) 2016 libedgegrid developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.
#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]
#![cfg_attr(feature = "clippy", deny(clippy, clippy_pedantic))]
#![cfg_attr(feature = "serde_macros", feature(custom_derive, plugin))]
#![cfg_attr(feature = "serde_macros", feature(custom_attribute))]
#![cfg_attr(feature = "serde_macros", plugin(serde_macros))]
#![deny(missing_docs)]
extern crate curl;
#[macro_use]
extern crate log;
extern crate url;
extern crate rustc_serialize;
extern crate serde;
extern crate serde_json;
extern crate sodium_sys;
extern crate time;

use curl::http;
use sodium_sys::crypto::utils::init;
use std::fmt;
use std::string;
use std::sync::{ONCE_INIT, Once};

mod request;
mod response;

pub mod auth;
#[cfg(feature = "ccu")]pub mod ccu;
#[cfg(feature = "luna")]pub mod luna;
pub mod version;

static START: Once = ONCE_INIT;

fn init() {
    START.call_once(|| {
        debug!("sodium_sys initialized");
        init::init();
    });
}

#[cfg(feature = "serde_macros")]
include!("lib.rs.in");

#[cfg(not(feature = "serde_macros"))]
include!(concat!(env!("OUT_DIR"), "/lib.rs"));

impl From<rustc_serialize::json::DecoderError> for EdgeGridError {
    fn from(e: rustc_serialize::json::DecoderError) -> EdgeGridError {
        EdgeGridError {
            title: Some(String::from("JSON DecoderError")),
            detail: Some(format!("{:?}", e)),
            ..Default::default()
        }
    }
}

impl From<rustc_serialize::json::EncoderError> for EdgeGridError {
    fn from(e: rustc_serialize::json::EncoderError) -> EdgeGridError {
        EdgeGridError {
            title: Some(String::from("JSON EncoderError")),
            detail: Some(format!("{:?}", e)),
            ..Default::default()
        }
    }
}

impl From<serde_json::error::Error> for EdgeGridError {
    fn from(e: serde_json::error::Error) -> EdgeGridError {
        EdgeGridError {
            title: Some(String::from("Serde JSON Error")),
            detail: Some(format!("{:?}", e)),
            ..Default::default()
        }
    }
}

impl From<auth::EdgeGridAuthError> for EdgeGridError {
    fn from(e: auth::EdgeGridAuthError) -> EdgeGridError {
        EdgeGridError {
            title: Some(String::from("EdgeGrid Authorization Header Error")),
            detail: Some(format!("{:?}", e)),
            ..Default::default()
        }
    }
}

impl From<curl::ErrCode> for EdgeGridError {
    fn from(e: curl::ErrCode) -> EdgeGridError {
        EdgeGridError {
            title: Some(String::from("curl error")),
            detail: Some(format!("{:?}", e)),
            ..Default::default()
        }
    }
}

impl From<string::FromUtf8Error> for EdgeGridError {
    fn from(e: string::FromUtf8Error) -> EdgeGridError {
        EdgeGridError {
            title: Some(String::from("FromUtf8Error")),
            detail: Some(format!("{:?}", e)),
            ..Default::default()
        }
    }
}

#[cfg(feature = "luna")]
fn to_str<T>(opt: &Option<T>) -> String
    where T: fmt::Display
{
    match *opt {
        Some(ref o) => format!("{}", o),
        None => String::new(),
    }
}

fn push_opt<T>(out: &mut String, prefix: &str, opt: &Option<T>)
    where T: fmt::Display
{
    if let Some(ref o) = *opt {
        out.push_str(&format!("{}{}\n", prefix, o)[..]);
    }
}

/// Akamai OPEN EdgeGrid Response Type
pub type EdgeGridResponse = Result<http::Response, EdgeGridError>;
/// Akamai OPEN EdgeGrid Result Type
pub type EdgeGridResult = Result<String, EdgeGridError>;

#[cfg(all(unix,test))]
const TEST_VER: [u8; 18] = [27, 91, 51, 50, 59, 49, 109, 108, 105, 98, 101, 100, 103, 101, 103,
                            114, 105, 100];

#[cfg(all(windows,test))]
const TEST_VER: [u8; 11] = [108, 105, 98, 101, 100, 103, 101, 103, 114, 105, 100];

#[test]
#[cfg(unix)]
fn version() {
    use sodium_sys::crypto::utils::secmem;
    let version = version::version(false);
    let vb = version.as_bytes();
    assert!(secmem::memcmp(&vb[..18], &TEST_VER) == 0);
}

#[test]
#[cfg(windows)]
fn version() {
    use sodium_sys::crypto::utils::secmem;
    let version = version::version(false);
    let vb = version.as_bytes();
    assert!(secmem::memcmp(&vb[..11], &TEST_VER) == 0);
}
