//! Akamai {OPEN} EdgeGrid Content Control Utility Request Handler
use auth::EdgeGridAuth;
use request;
use request::HttpRequestVerb::*;
use serde_json;
use std::str::FromStr;

#[cfg(feature = "serde_macros")]
include!("req.rs.in");

#[cfg(not(feature = "serde_macros"))]
include!(concat!(env!("OUT_DIR"), "/ccu/req.rs"));

impl PurgeArls {
    /// Create a new PurgeArls struct with the given vector of ARLs.
    pub fn new(arls: Vec<String>) -> PurgeArls {
        PurgeArls { objects: arls }
    }
}

impl PurgeCPCodes {
    /// Create a new PurgeCPCodes struct.
    pub fn new(cpcodes: Vec<String>) -> PurgeCPCodes {
        let mut codes: Vec<usize> = Vec::new();

        for code in &cpcodes {
            match usize::from_str(code) {
                Ok(c) => {
                    codes.push(c);
                }
                Err(e) => {
                    error!("{:?}", e);
                }
            }
        }

        PurgeCPCodes {
            objects: codes,
            action: "remove".to_owned(),
            purge_type: "cpcode".to_owned(),
        }
    }
}

const BASE_URL: &'static str = "/ccu/v2/";

fn purge(edge_grid_auth: &EdgeGridAuth, queue_name: &str, json: &str) -> ::EdgeGridResponse {
    let relurl = &format!("{}queues/{}", BASE_URL, queue_name)[..];
    Ok(try!(request::request(edge_grid_auth, relurl, Some(json), POST)))
}

/// Purge by ARLs.
pub fn purge_by_arl(edge_grid_auth: &EdgeGridAuth,
                    queue_name: &str,
                    body: PurgeArls)
                    -> ::EdgeGridResponse {
    let json = &try!(serde_json::to_string(&body))[..];
    purge(edge_grid_auth, queue_name, json)
}

/// Purge by CPCodes.
pub fn purge_by_cpcode(edge_grid_auth: &EdgeGridAuth,
                       queue_name: &str,
                       body: PurgeCPCodes)
                       -> ::EdgeGridResponse {
    let json = &try!(serde_json::to_string(&body))[..];
    purge(edge_grid_auth, queue_name, json)
}

/// Check a Purge status.
pub fn purge_status(edge_grid_auth: &EdgeGridAuth, purge_id: &str) -> ::EdgeGridResponse {
    let relurl = &format!("{}purges/{}", BASE_URL, purge_id)[..];
    Ok(try!(request::request(edge_grid_auth, relurl, None, GET)))
}

/// Check a purge queue length.
pub fn queue_length(edge_grid_auth: &EdgeGridAuth, queue_name: &str) -> ::EdgeGridResponse {
    let relurl = &format!("{}queues/{}", BASE_URL, queue_name)[..];
    Ok(try!(request::request(edge_grid_auth, relurl, None, GET)))
}
