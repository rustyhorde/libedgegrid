//! Akamai OPEN EdgeGrid CCU Response
use curl::http;
use response::{gen_error_output, gen_output, parse_response};
use std::fmt;

use self::CCUResponseType::*;

/// CCU Response Types
pub enum CCUResponseType {
    /// Purge Response
    Purge,
    /// Purge Status Response
    PurgeStatus,
    /// Queue Length Response
    QueueLength,
}

#[cfg(feature = "serde_macros")]
include!("resp.rs.in");

#[cfg(not(feature = "serde_macros"))]
include!(concat!(env!("OUT_DIR"), "/ccu/resp.rs"));

/// Parse the given respose based on the response type expected.
pub fn parse(resp: http::Response, ccu_type: CCUResponseType) -> ::EdgeGridResult {
    match ccu_type {
        Purge => parse_response(resp, gen_output::<PurgeResponse>, gen_error_output),
        PurgeStatus => parse_response(resp, gen_output::<PurgeStatusResponse>, gen_error_output),
        QueueLength => parse_response(resp, gen_output::<QueueLengthResponse>, gen_error_output),
    }
}
