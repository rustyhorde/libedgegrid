// build.rs
extern crate vergen;

use vergen::*;

macro_rules! stderr(
    ($($arg:tt)*) => (
        match writeln!(&mut ::std::io::stderr(), $($arg)+ ) {
            Ok(_) => {},
            Err(x) => panic!("Unable to write to stderr: {}", x),
        }
    )
);

#[cfg(not(feature = "serde_macros"))]
mod inner {
    extern crate glob;
    extern crate syntex;
    extern crate serde_codegen;

    use std::env;
    use std::fs;
    use std::io::Write;
    use std::path::Path;
    use std::result::Result;

    pub fn main() {
        let out_dir = match env::var("OUT_DIR") {
            Ok(d) => d,
            Err(e) => panic!("Cannot find OUT_DIR: {}", e),
        };

        for path in glob::glob("src/**/*.in").unwrap().filter_map(Result::ok) {
            // Save the source path for later.
            let src = path.clone();

            // Break off the path prefix (update when relative_from is stable).
            let mut components = path.components();
            components.next();

            // A temp output path (the filename is incorrect).
            let temp = Path::new(&out_dir).join(components.as_path());

            // Grab the parent path.
            let parent = match temp.parent() {
                Some(p) => {
                    let _ = fs::create_dir_all(p);
                    p
                }
                None => panic!("Could not determine parent!"),
            };

            // Setup the correct output file name.
            let out_file_name = match temp.file_stem() {
                Some(fs) => fs,
                None => panic!("Unable to determine file stem!"),
            };

            // Save the correct destination path.
            let dst = parent.join(out_file_name);
            stderr!("SRC: {}, DST: {}", src.display(), dst.display());

            // Run the serde codegen.
            let mut registry = syntex::Registry::new();
            serde_codegen::register(&mut registry);
            registry.expand("", &src, &dst).unwrap();
        }
    }
}

#[cfg(feature = "serde_macros")]
mod inner {
    pub fn main() {}
}

fn main() {
    let mut flags = OutputFns::all();
    flags.toggle(NOW);
    assert!(vergen(flags).is_ok());
    inner::main();
}
