# libedgegrid
Akamai {OPEN} EdgeGrid API Request/Response Handler

## Version
[![Crates.io](https://img.shields.io/crates/v/libedgegrid.svg)](https://crates.io/crates/libedgegrid)

## Status
[![Build Status](https://travis-ci.org/rustyhorde/libedgegrid.svg?branch=master)](https://travis-ci.org/rustyhorde/libedgegrid)
[![Coverage Status](https://coveralls.io/repos/github/rustyhorde/libedgegrid/badge.svg?branch=master)](https://coveralls.io/github/rustyhorde/libedgegrid?branch=master)

## Documentation
[libedgegrid](http://rustyhorde.github.io/libedgegrid/libedgegrid/libedgegrid/index.html)

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
